const mongoose = require("mongoose");

const entradaSchema = new mongoose.Schema({
  titulo: {
    type: String
  },
  claves: [
      {
          type: String
      }
  ],
  descripcion: {
      type: String
  },
  tipo: {
      type: String
  }
});

const Entrada = mongoose.model("Entrada", entradaSchema);

module.exports = Entrada;
