const express = require("express");
const app = express();
const connectDb = require("./src/connection");
const Entrada = require("./src/Entrada.model");
const cors = require('cors');
const isEmpty = require("./validation/is-empty");
const bodyParser = require('body-parser')

app.use(cors());
app.use(bodyParser.json());

const PORT = 8080;

// @route   GET /entradas
// @desc    dar todas las entradas
// @access  Public
app.get("/entradas", async (req, res) => {
  const entradas = await Entrada.find({});

  res.json(entradas);
});


// @route   POST /crear-entrada
// @desc    crear nueva entrada
// @access  Public
app.post("/crear-entrada.json", async (req, res) => {
  const entradaFields = {};
  const errors = {};

  console.log(req);

  if(req.body.titulo) entradaFields.titulo = req.body.titulo;
  if(req.body.descripcion) entradaFields.descripcion = req.body.descripcion;
  if(req.body.tipo) entradaFields.tipo = req.body.tipo;

  if(req.body.claves){
    var arrayClaves = req.body.claves.split(',');

    entradaFields.claves = arrayClaves;
  }

  Entrada.findOne({titulo: req.body.titulo}).then(entrada => {
    if(entrada){
      // la entrada ya existe se puede actualizar
      errors.update = 'Ya existe una entrada con ese titulo';

      res.json(errors);
    } else {
      // la entrada es una nueva entrada
      new Entrada(entradaFields).save().then(entrada => {
        res.json(entrada)
      })
    }
  })
  
});

app.listen(PORT, function() {
  console.log(`Corriendo en el puerto ${PORT}`);

  connectDb().then(() => {
    console.log("Base de datos conectada");
  });
});
