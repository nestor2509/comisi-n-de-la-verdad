import React, {Component} from 'react';
import axios from 'axios';
import './App.scss';

import Listado from './componentes/Listado';

const apiUrl = `http://localhost:8080`;

class App extends Component {

constructor(props){
  super(props);
  this.state = {
    entradas: [],
    titulo: '',
    claves: '',
    descripcion: '',
    tipo: 'Testimonio'
  }

  this.cargarEntradas = this.cargarEntradas.bind(this);
  this.onChange = this.onChange.bind(this);
  this.onSubmit = this.onSubmit.bind(this);
}

componentDidMount(){
  this.cargarEntradas();
}
 

  async cargarEntradas() {
    const res = await axios.get(apiUrl + '/entradas');
    this.setState({
      entradas: res.data
    })
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  async onSubmit(){
    const nuevaEntrada = {
      titulo: this.state.titulo,
      claves: this.state.claves,
      descripcion: this.state.descripcion,
      tipo: this.state.tipo
    };

    const params = new URLSearchParams();
    params.append('titulo', this.state.titulo);
    params.append('claves', this.state.claves);
    params.append('descripcion', this.state.descripcion);
    params.append('tipo', this.state.tipo);

    axios({
      method: 'post',
      headers: {
        "Content-Type": "application/json",
      },
      url: apiUrl + '/crear-entrada.json',
      data: nuevaEntrada
    }).then(res => {
      console.log(res);
      this.cargarEntradas();
    });

  }

  render(){
    return (
      <div className="App">
        <h1>Prueba de selección Comisión de la Verdad</h1>
        <div className="inside">
          <div className="left">
            <h2>Listado de entradas</h2>
            <Listado entradas={this.state.entradas} />
          </div>
          <div className="right">
            <h2>Agregar entrada</h2>
    
            <form>
              <p>
                <label htmlFor="titulo">Titulo</label>
                <input type="text" onChange={this.onChange} name="titulo" id="titulo"/>
              </p>
              <p>
                <label htmlFor="claves">Claves (separadas por comas)</label>
                <input type="text" onChange={this.onChange} name="claves" id="claves"/>
              </p>
              <p>
                <label htmlFor="descripcion">Descripcion</label>
                <textarea onChange={this.onChange} name="descripcion" id="descripcion" cols="30" rows="10"></textarea>
              </p>
              <select onChange={this.onChange} name="tipo" id="tipo-recurso">
                <option value="Testimonio">Testimonio</option>
                <option value="Informe">Informe</option>
                <option value="Caso">Caso</option>
              </select>
              <input type="submit" onClick={e => {e.preventDefault(); this.onSubmit()}} value="Añadir entrada"/>
            </form>
          </div>
        </div>
        
      </div>
    );
  }
}

export default App;
