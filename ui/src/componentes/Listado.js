import React, {Component} from 'react';

class Listado extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (<ul>
                <li className="top">
                    <div className="titulo">Título</div>
                    <div className="claves">Claves</div>
                    <div className="des">Descripcion</div>
                    <div className="tipo">Tipo</div>
                </li>
            {this.props.entradas.map(entrada => (
                <li key={entrada._id}>
                    <div className="titulo">{entrada.titulo}</div>
                    <div className="claves">{entrada.claves.join(', ')}</div>
                    <div className="des">{entrada.descripcion}</div>
                    <div className="tipo">{entrada.tipo}</div>
                </li>
            ))}
        </ul>)
    }
}

export default Listado;