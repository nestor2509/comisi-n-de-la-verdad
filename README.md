# Entrega

Para inicializar con docker, ejecutar en la carpeta raíz:

```bash
docker-compose up -d
```

Para reconstruir los contenedores:

```bash
docker-compose up --build -d
```


La configuración de cada contenedor se encuentra en la carpeta del aplicativo en el archivo Dockerfile, ya sea api o ui. Se utilizó el mismo Dockerfile para ambos partiendo del contenedor node versión 8. Se crea una carpeta en el contenedor /usr/src/app y en ella se copia el package.json que tiene la lista de los paquetes y se instalan con npm install. Luego se copia todo el contenido de la carpeta a la carpeta /usr/src/app del contenedor, se expone el puerto que se va a usar y se corre el aplicativo con npm start. En /api se encuentra la configuración del api que se usa para hablar con mongo y en /ui la app de react.

El aplicativo en react corre en el puerto 3000 y el api en el puerto 8080 de localhost si se corre en local.

La ubicación geográfica de cada entrada se puede usar un mapa con coordenadas decimales o partir de una implementación usando las bases de datos del divipola y del lado del front se usan esas categorías para ubicar las entradas en un mapa, eso queda pendiente.

Gracias.